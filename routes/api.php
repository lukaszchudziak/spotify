<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("songs/save", "SongController@addSong");
Route::post("songs/{song}", "SongController@getSong");
Route::post("songs/{song}/delete", "SongController@deleteSong");
Route::post("songs/{song}/update", "SongController@updateSong");
Route::post("songs", "SongController@getSongs");
Route::post("eksport", "SongController@eksport");
Route::post("import", "SongController@import");
