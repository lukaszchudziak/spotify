<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Category;
use App\Models\Song;
use Illuminate\Http\Request;

class SongController extends Controller
{
    public function getSongs(){
        return Song::with("author","category")->get();
    }

    public function updateSong(Request $request, Song $song){
        $song->update($request->all());
        return 200;

    }

    public function addSong(Request $request){
        Song::create($request->all());
        return 200;
    }

    public function getSong(Song $song){
        return $song;
    }

    public function deleteSong(Song $song){
        $song->delete();
        return 200;
    }

    public function import(Request $request){
        Category::insert($request->category);
        Author::insert($request->author);
        Song::insert($request->song);
        return 200;
    }

    public function eksport(Request $request){
        return  ["song" => Song::all(), "category" => Category::all(), "author" => Author::all()];
    }
}
