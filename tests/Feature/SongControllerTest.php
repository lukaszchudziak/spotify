<?php

namespace Tests\Feature;

use Tests\TestCase;

use App\Models\Song;


class SongControllerTest extends TestCase
{
    public function test_addSong(){
        $response = $this->postJson('api/songs/save', ["author_id" => "1", "category_id" => "1", "name" => "testowa", "time" => "4:20"]);

        $response
            ->assertStatus(200);
    }
    public function test_getSongs(){
        $response = $this->postJson('/api/songs');

        $response
            ->assertStatus(200);
    }

    public function test_updateSong(){
        $response = $this->postJson('/api/songs/'.Song::inRandomOrder()->first()->id."/update", ["name" => "test2"]);

        $response
            ->assertStatus(200);
    }


    public function test_getSong(){
        $response = $this->postJson('/api/songs/'.Song::inRandomOrder()->first()->id);

        $response
            ->assertStatus(200);
    }

    public function test_deleteSong(){
        $response = $this->postJson('/api/songs/'.Song::inRandomOrder()->first()->id."/delete");

        $response
            ->assertStatus(200);
    }
}
